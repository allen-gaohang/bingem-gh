# -*-coding:utf-8-*-
"""
用于生成ID的工具
"""
import mmh3
import struct


def construct_id(str_list=[], seed=1234, signed=True):
    if None in str_list:
        return None
    h = mmh3.hash128(''.join(str_list), seed, signed=signed)
    bs = biginteger_2_bytelist(h)
    return abs(bytelist_as_long(bs))


def biginteger_2_bytelist(bi):
    return struct.unpack('16b', bi.to_bytes(16, byteorder="big", signed=True))


def bytelist_as_long(bs=[]):
    l = int(bs[0]) & 255
    size = min(len(bs), 8)
    for i in range(size):
        l = l | (int((bs[i]) & 255) << i *8)
    return abs(l) if l < 9223372036854775808 else abs(l-2*9223372036854775808)


def string_as_long(s = ''):
    bs = [ord(i) for i in s]
    return bytelist_as_long(bs)

def test_gen_id(file_data):
    bin_id = str(file_data['bin_id'])

if __name__ == '__main__':

    id = construct_id(['1234'])
    print(id)

    md5 = '5AD668E364D2141F1D3B46D7298AD9B3'
    md5 = None
    label = '.init_proc'
    sta_ea = 13824
    end_ea = 13850

    id = construct_id([md5])
    print(id)

    id = construct_id([md5, label, str(sta_ea), str(end_ea)])
    print(id)


