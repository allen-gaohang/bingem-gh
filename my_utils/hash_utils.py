import hashlib


def file_sha256(filepath):
    with open(filepath, 'rb') as f:
        shaObj = hashlib.sha256()
        shaObj.update(f.read())
        hashCode = shaObj.hexdigest()
        return hashCode


def file_sha512(filepath):
    with open(filepath, 'rb') as f:
        shaObj = hashlib.sha512()
        shaObj.update(f.read())
        hashCode = shaObj.hexdigest()
    return hashCode


def file_md5(filepath):
    with open(filepath, 'rb') as f:
        md5obj = hashlib.md5()
        md5obj.update(f.read())
        hashCode = md5obj.hexdigest()
    return hashCode


def check_hash_conflict(root_path, hash_type="md5"):
    from file_utils import get_files_ext
    hcode2fname = {}
    paths = get_files_ext(root_path, filtered_suf=(".pkl", ".dat", ".idb", ".i64"))
    for f in paths:
        if hash_type=="md5":
            hashCode = file_md5(f)
        elif hash_type=="file_256":
            hashCode = file_sha256(f)
        elif hash_type=="file_512":
            hashCode = file_sha512(f)
        else:
            hashCode = file_md5(f)
        if hashCode in hcode2fname.keys():
            hcode2fname[hashCode].append(f)
        else:
            hcode2fname[hashCode] = [f]
    dup_hash, dup_file = 0, 0
    for k, v in hcode2fname.items():
        if len(v) > 1:
            dup_file += len(v)
            dup_hash += 1
            print("conflict files:")
            for dup_f in v:
                print("\t\t"+dup_f)
    print("Total files:{}, conflict hashes:{}, conflict files: {}".format(len(paths), dup_hash, dup_file))
    return dup_hash, dup_file



if __name__ == "__main__":
    # file_path =  "D:/workspace/bingem-gh/database/binaries/b2sum_O2_Coreutils"
    # r = file_sha256(file_path)
    # print(r)

    root_path = "U:\\binsearch_data\\二进制文件"
    check_hash_conflict(root_path, "sha256")


