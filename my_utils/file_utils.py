import shutil
import os
import subprocess
import time
from pathlib import Path
from tqdm import tqdm
import random


# filter_file = (".i64", "til", "nam", ".idb", ".id0", ".id1", ".id2", ".pkl", ".py", ".txt", ".dat", ".a",
# ".sqlite", ".sqlite-crash", ".o") filtered_list = [_ for _ in fileList if not _.endswith(filter_file) and not
# os.path.isdir(_)]

def get_sub_dirs(root, topk=8):
    fileList = os.listdir(root)
    abs_path_lst = []
    for ith, path in enumerate(fileList):
        if ith > topk:
            break
        abs_path = os.path.join(root, path).replace('\\', '/')
        if os.path.isdir(abs_path):
            abs_path_lst.append(abs_path)
    return abs_path_lst


def cp_files(src_dir, des_dir, selected_suf=(".pkl")):
    '''
    copy files from src to des
    '''
    os.makedirs(des_dir, exist_ok=True)
    file_lst = []
    if des_dir[-2:] != "\\" and des_dir[-1] != '/':
        des_dir += "/"
    for x in Path(src_dir).glob("**/*"):
        if os.path.isfile(x) and str(x).endswith(selected_suf):
            file_lst.append(str(x))
    print("total:{}, copy {} to {}".format(len(file_lst), selected_suf, des_dir))
    filenames = [x.split("\\")[-1] for x in file_lst]
    for f_abst, f_rela in zip(file_lst, filenames):
        shutil.copy(f_abst, des_dir + f_rela)


def mv_files(src_dir, des_dir, selected_suf=(".pkl")):
    '''
    copy files from src to des
    '''
    os.makedirs(des_dir, exist_ok=True)
    file_lst = []
    if des_dir[-2:] != "\\" and des_dir[-1] != '/':
        des_dir += "/"
    for x in Path(src_dir).glob("**/*"):
        if os.path.isfile(x) and str(x).endswith(selected_suf):
            file_lst.append(str(x))
    print("total:{}, move {} files to {}".format(len(file_lst), selected_suf, des_dir))
    filenames = [x.split("\\")[-1] for x in file_lst]
    for f_abst, f_rela in zip(file_lst, filenames):
        shutil.move(f_abst, des_dir + f_rela)


def cp_tree_ext(src, des, selected_suf):
    '''
    copy files recursively keep relative directory path name
    '''
    fp = {}
    exts = selected_suf.lower().split()
    for dn, dns, fns in os.walk(src):
        for fl in fns:
            if os.path.splitext(fl)[1] in exts:
                if dn not in fp.keys():
                    fp[dn] = []
                fp[dn].append(fl)
        for k, v in fp.items():
            relative_path = k[len(src) + 1:]
            new_path = os.path.join(des, relative_path)
            for f in v:
                old_path = os.path.join(k, f)
            print("copy [" + old_path + "] to [" + new_path + "]")
            if not os.path.exists(new_path):
                os.makedirs(new_path)
            shutil.copy(old_path, new_path)


def mv_tree_ext(src, des, selected_suf):
    '''
        move files recursively keep relative directory path name
    '''
    fp = {}
    exts = selected_suf.lower().split()
    for dn, dns, fns in os.walk(src):
        for fl in fns:
            if os.path.splitext(fl)[1] in exts:
                if dn not in fp.keys():
                    fp[dn] = []
                fp[dn].append(fl)
        for k, v in fp.items():
            relative_path = k[len(src) + 1:]
            new_path = os.path.join(des, relative_path)
            for f in v:
                old_path = os.path.join(k, f)
            print("move [" + old_path + "] to [" + new_path + "]")
            if not os.path.exists(new_path):
                os.makedirs(new_path)
            shutil.move(old_path, new_path)


def get_dir_by_level(path, idx):
    path_segs = path.split("\\")
    dir = path_segs[idx % len(path_segs)]
    return dir


def get_files_ext(root_path, selected_suf=(), filtered_suf=('.i64', '.txt', 'idb'), begin=None):
    '''
    list all bin files in root directory
    1) according to filtered_suf
    2) according to selected_suf
    '''

    all_files = []
    if isinstance(root_path, str):
        if len(selected_suf) > 0:
            file_iter = Path(root_path).glob("**/*")
            for x in file_iter:
                if os.path.isfile(x) and str(x).endswith(selected_suf) and not str(x).endswith(filtered_suf):
                    all_files.append(str(x))
        else:
            for x in Path(root_path).glob("**/*"):
                if os.path.isfile(x) and not str(x).endswith(filtered_suf):
                    all_files.append(str(x))
    elif isinstance(root_path, list):
        for path in root_path:
            if len(selected_suf) > 0:
                file_iter = Path(path).glob("**/*")
                for x in file_iter:
                    name = str(x)
                    if os.path.isfile(name) and str(name).endswith(selected_suf) and not str(name).endswith(
                            filtered_suf):
                        all_files.append(name)
            else:
                for x in Path(path).glob("**/*"):
                    if os.path.isfile(x) and not str(x).endswith(filtered_suf):
                        all_files.append(str(x))
    if begin:
        begin_i = 0
        for i, elem in enumerate(all_files):
            if elem == begin:
                begin_i = i
        all_files = all_files[begin_i:]
    return all_files


def get_subpath_ext(root_path: str, search_type="*", selected_suf=(),
                    filtered_suf=(".ida", ".id0", ".id1", ".id2", ".id3", ".i64", ".txt", ".dat"), only_path=None):
    """
    "*" all file's and first sub directory's paths
    "**" all first sub directory's paths
    "**" all dirs' paths recursively
    "*/*" all secondary sub directory's paths
    "**/*" all files' and dirs' paths recursively
    """
    dir_lst, file_lst = [], []
    for p in Path(root_path).glob(search_type):
        if Path(p).is_file():
            file_lst.append(p)
        else:
            dir_lst.append(p)
    if len(selected_suf) > 0:
        file_lst = [x for x in file_lst if str(x).endswith(selected_suf)]
    if len(filtered_suf) > 0:
        file_lst = [x for x in file_lst if not str(x).endswith(filtered_suf)]
    if only_path:
        file_lst = [x for x in file_lst if only_path in str(x).split("\\")]
        dir_lst = [x for x in dir_lst if only_path in str(x).split("\\")]
    return dir_lst, file_lst


def get_files_by_dir(root, selected_dir, file_filter=(".pkl", ".idb")):
    ret_files = []
    candidate_files = get_files_ext(root_path=root, filtered_suf=file_filter)
    for f in candidate_files:
        relative_path = f.replace(root, "").strip("\\")
        path_segs = relative_path.split("\\")
        if len(path_segs) <= 1:
            continue
        dir_segs = path_segs[:-1]
        all_sel_exist = True
        for d in selected_dir:
            if d not in dir_segs:
                all_sel_exist = False
        if all_sel_exist:
            ret_files.append(f)
    return ret_files


def modify_path_by_dir_concat(path="D:\\workspace\\bingem-gh\\database\\finetune\\1\\b2sum_O3_Coreutils.hash.dat", \
                              new_root="Z:\\workspace\\bingem-gh", concat_idx=[-4, -2], keep_idx=[-3], concat_str="_"):
    path_segs = path.split("\\")
    concat_idx = [_ % len(path_segs) for _ in concat_idx]
    keep_idx = [_ % len(path_segs) for _ in keep_idx]
    concat_dirs = [x for i, x in enumerate(path_segs) if i in concat_idx]
    keep_dirs = [x for i, x in enumerate(path_segs) if i in keep_idx]
    f_name = path_segs[-1]
    new_path = "{0}\\{1}\\{2}\\{3}".format(new_root, "\\".join(keep_dirs), concat_str.join(concat_dirs), f_name)
    return new_path


if __name__ == '__main__':
    new_root = "U:\\binsearch_data\\train\\dtd\\luo\\gcc"
    # old_roots = ["U:\\binsearch_data\\train\\dtd\\1", "U:\\binsearch_data\\train\\dtd\\2"]
    # all_files = get_files_ext(root_path=old_roots, selected_suf=".pkl")
    all_files = {}
    _, all_O0_files = get_subpath_ext(root_path=new_root, search_type="**/*", selected_suf=".pkl", only_path="O0")
    _, all_O1_files = get_subpath_ext(root_path=new_root, search_type="**/*", selected_suf=".pkl", only_path="O1")
    _, all_O2_files = get_subpath_ext(root_path=new_root, search_type="**/*", selected_suf=".pkl", only_path="O2")
    _, all_O3_files = get_subpath_ext(root_path=new_root, search_type="**/*", selected_suf=".pkl", only_path="O3")
    anchor_names = [os.path.basename(x) for x in all_O0_files]
    name2path = {}
    name2path["O0"] = dict(zip([os.path.basename(x) for x in all_O0_files], all_O0_files))
    name2path["O1"] = dict(zip([os.path.basename(x) for x in all_O1_files], all_O1_files))
    name2path["O2"] = dict(zip([os.path.basename(x) for x in all_O2_files], all_O2_files))
    name2path["O3"] = dict(zip([os.path.basename(x) for x in all_O3_files], all_O3_files))
    n_pairs = 0
    n_anchor = 2
    for name0, path0 in random.sample(name2path["O0"].items(), n_anchor):
        opt = random.choice(["O2", "O3"])
        path23 = name2path[opt].get(name0, None)
        if path23:
            (path0, path23)
            n_pairs += 1

    # for old_path in all_files:
    #     new_path = modify_path_by_dir_concat(path=old_path, new_root=new_root)
    #     print("{}  ->  {}".format(old_path, new_path))
    #     os.makedirs(os.path.dirname(new_path), exist_ok=True)
    #     shutil.copy(old_path, new_path)
