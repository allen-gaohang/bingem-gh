import os
import subprocess
from tqdm import tqdm
import time
from pathlib import Path


def call_ida(ida_abs_path: str, py_path: str, bin_paths: list, last_pause=None):
    """
    according to alphabetic order, from begin to last, call ida
    """
    if not os.path.exists(py_path):
        print("script not exists")
        return
    bin_paths.sort()
    if last_pause:
        bin_paths_from_begin = [x for x in bin_paths if x >= last_pause]
    else:
        bin_paths_from_begin = bin_paths
    bar = tqdm(iterable=bin_paths_from_begin, desc='bin parsing progress', unit='file')
    for f in bar:
        if Path(f).is_file():
            # cur = time.time()
            cmd_str = '"{}" -A -S"{}" "{}"'.format(ida_abs_path, py_path, f)
            print("\n", cmd_str)
            res = subprocess.Popen(cmd_str, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                                   stderr=subprocess.PIPE)
            # result = res.stdout.read()
            res.wait()
            res.stdout.close()
            # print("cmd output: ", result)
            # time.sleep(0.1)
            # elapsed = time.time() - cur
            # print("elapsed: {}s".format(elapsed))
        else:
            print("file error", f)