import pickle


path = "D:/workspace/BinGEM/database/binaries/train/b2sum_O2_Coreutils.addr.dat"
data = pickle.load(open(path, "rb"))
print(data)

# .addr.dat : dict key:address, value: srcline = concat(source_file_name,line_number)
#   BatchExtractDwarf.py 用到 ExtractBlocks.py, 其中使用了 srcline
# usage:

# .hash.dat 块信息，dict  key: 源码, value: 汇编代码
# usage:  block triplet data

# .cmp.dat :  多个dict，记录了每个函数的信息，包括：调用关系 string 导入导出函数
# (callgraphs, STRINGS_DICT, STRINGS_LIST, imports_map, function_map, func_type_map, func_type_dict)
#  usage: no

# fcg.dat: 调用关系
#  usage: no

# .line.dat 函数信息，dict  key：函数名，value：函数信息
#  usage: no
