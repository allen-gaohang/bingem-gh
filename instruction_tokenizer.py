import pickle
import re
from tqdm import tqdm
import torch
import numpy as np
from my_utils.file_utils import get_files_ext


def is_torch_available():
    return True


def to_py_obj(obj):
    """
    Convert a TensorFlow tensor, PyTorch tensor, Numpy array or python list to a python list.
    """

    if isinstance(obj, (list, tuple)):
        return [to_py_obj(o) for o in obj]
    elif is_torch_available() and isinstance(obj, torch.Tensor):
        return obj.detach().cpu().tolist()
    elif isinstance(obj, np.ndarray):
        return obj.tolist()
    else:
        return obj


class InstructionTokenizer:
    def __init__(self, min_blk=5, statics=True):
        self.min_blk = min_blk
        self.TokenMap = {}
        self.OpcodeMap = {}
        self.OprandMap = {}
        self.statics = statics
        self.id = len(self.TokenMap)
        self.Opcode_id = len(self.OpcodeMap)
        self.Oprand_id = len(self.OprandMap)
        self.initMap()

    def initMap(self):
        if len(self.TokenMap) < 5:
            bos_token = "<s>",
            eos_token = "</s>",
            sep_token = "</s>",
            cls_token = "<s>",
            unk_token = "<unk>",
            pad_token = "<pad>",
            mask_token = "<mask>",
            special_token = [
                "<s>",
                "<pad>",
                "</s>",
                "<unk>",
                "<mask>",
            ]
            self.mask_token = "<mask>"
            self.pad_token = "<pad>"
            for i in special_token:
                self.insert_token(i)
            self.pad_token_id = self.TokenMap[self.pad_token]
            self.all_special_ids = [_ for _ in range(len(self.TokenMap))]
        else:
            pass

    def normalize_opcode(self, opcode):
        mov_suffix = ("mov")
        if opcode.startswith(mov_suffix):
            return "mov"
        return opcode

    def normalize_oprand(self, oprands):
        cs_tag = "tagcs"
        ds_tag = "tagds"
        loc_tag = "tagloc"
        locret_tag = "taglocret"
        ptr_tag = "tagptr"
        addr_tag = "tagaddr"
        other_tag = "tagothers"
        bad_words = ["_", "qword ptr ", "offset "]

        ret = []
        for opr in oprands:
            if "cs:" in opr:
                opr = cs_tag
                ret.append(opr)
                continue
            if "ds:" in opr:
                opr = ds_tag
                ret.append(opr)
                continue
            elif "loc_" in opr:
                opr = loc_tag
                ret.append(opr)
                continue
            elif "locret_" in opr:
                opr = locret_tag
                ret.append(opr)
                continue
            elif " ptr " in opr:  # "word ptr", "byte ptr"
                opr = ptr_tag
                ret.append(opr)
                continue
            elif opr.startswith("[") and opr.endswith("]"):
                opr = addr_tag
                ret.append(opr)
                continue
            elif len(opr) >= 3:
                # opr = re.sub(r"[A-Fa-f0-9]{4,18}[h]?", "LNUM", opr)
                opr = re.sub(r"(?<![0-9a-wyzA-Z_.])([A-Fa-f0-9]{4,18}[h]?)(?![0-9a-zA-Z_])", "LNUM",
                             opr)  # 修改正则表达式，加入断言
                if "LNUM" in opr:  # 字符串以a开头
                    continue
                else:
                    # opr = re.sub(r"[A-Fa-f0-9]{2,4}[h]", "SNUM", opr)
                    opr = re.sub(r"(?<![0-9a-wyzA-Z_.])([A-Fa-f0-9]{2,4}[h])(?![0-9a-zA-Z_])", "SNUM", opr)
                if "LNUM" in opr or "SNUM" in opr:
                    ret.append(opr)
                    continue
                else:
                    pass
            else:
                pass
            if len(opr) > 8:
                opr = other_tag
            ret.append(opr)
        return ret

    def get_special_tokens_mask(self, token_ids_0, already_has_special_tokens=None):
        all_special_ids = self.all_special_ids

        special_tokens_mask = [1 if token in all_special_ids else 0 for token in token_ids_0]

        return special_tokens_mask

    def convert_tokens_to_ids(self, token):
        return self.TokenMap[token]

    def update(self, pkl_file_lst):
        pbar = tqdm(pkl_file_lst)
        for f in pbar:
            self._update_by_file(f)

    def _update_by_file(self, f):
        FunctionList = pickle.load(open(f, "rb"))
        for func in FunctionList:
            if len(func['blocks']) < self.min_blk:
                continue
            for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
                instructions = func['blocks'][cur]['src']
                for each_ins in instructions:
                    opcode, oprands = self.normalize(each_ins)
                    if self.statics:
                        num = self.OpcodeMap.get(opcode, 0) + 1
                        self.OpcodeMap[opcode] = num
                        for oprand in oprands:
                            num = self.OprandMap.get(oprand, 0) + 1
                            self.OprandMap[oprand] = num
                    token = opcode
                    if len(oprands) > 0:
                        token = token + "~" + "~".join(oprands)
                    self.insert_token(token)

    def normalize(self, each_ins):
        ## ignore oprand keep opcode only
        opcode = each_ins[0]
        return self.normalize_opcode(opcode), []

    # def normalize(self, each_ins):
    #     opcode = each_ins[0]
    #     oprands = each_ins[1:]
    #     return self.normalize_opcode(opcode), self.normalize_oprand(oprands)

    def insert_token(self, token):
        if token in self.TokenMap:
            pass
        else:
            self.TokenMap[token] = self.id
            self.id += 1

    def pad(self, encoded_inputs, return_tensors=None, pad_length=None):
        encoded_inputs = {key: [example[key] for example in encoded_inputs] for key in encoded_inputs[0].keys()}
        assert "input_ids" in encoded_inputs, (
            "You should supply an encoding or a list of encodings to this method. "
            "An encoding is the output of one the encoding methods of the tokenizer, i.e. "
            "__call__/encode_plus/batch_encode_plus. "
        )
        first_element = encoded_inputs["input_ids"][0]
        for key, value in encoded_inputs.items():
            encoded_inputs[key] = to_py_obj(value)

        batch_size = len(encoded_inputs["input_ids"])
        if pad_length:
            max_length = pad_length
        else:
            max_length = max(len(inputs) for inputs in encoded_inputs["input_ids"])

        batch_outputs = {}
        for i in range(batch_size):
            inputs = dict((k, v[i]) for k, v in encoded_inputs.items())
            outputs = self._pad(
                inputs,
                max_length=max_length,
            )

            for key, value in outputs.items():
                if key not in batch_outputs:
                    batch_outputs[key] = []
                batch_outputs[key].append(value)
        batch_outputs['input_ids'] = torch.tensor(batch_outputs['input_ids'])
        return batch_outputs

    def _pad(self, encoded_inputs, max_length=None, return_attention_mask=None):
        difference = max_length - len(encoded_inputs["input_ids"])

        if return_attention_mask:
            encoded_inputs["attention_mask"] = [1] * len(encoded_inputs["input_ids"]) + [0] * difference
        if "token_type_ids" in encoded_inputs:
            encoded_inputs["token_type_ids"] = (
                    encoded_inputs["token_type_ids"] + [self.pad_token_type_id] * difference
            )
        encoded_inputs["input_ids"] = encoded_inputs["input_ids"] + [self.pad_token_id] * difference

        return encoded_inputs

    def __call__(self, insturction_seq, return_tensors=True, max_length=256, return_dict=True, padding=False,
                 truncation=False, **kwargs):
        ret = None
        if truncation:
            insturction_seq = insturction_seq[:max_length]
        tokens = []
        tokens.append(self.TokenMap["<s>"])
        for each_ins in insturction_seq:
            opcode, oprands = self.normalize(each_ins)
            token = opcode + "~" + "~".join(oprands)
            if token in self.TokenMap:
                tokens.append(self.TokenMap[token])
            else:
                tokens.append(self.TokenMap["<unk>"])
        tokens.append(self.TokenMap["</s>"])
        if return_dict:
            ret = {'input_ids': torch.tensor(tokens)}
        else:
            ret = torch.tensor(tokens).reshape(1, len(tokens))
        return ret

    def __len__(self):
        return len(self.TokenMap)


if __name__ == '__main__':
    # tokenizer_ckp = "checkpoints/tokenizer/luo/tokenizer.ckp"
    # root_path = "U:\\binsearch_data\\train\\luo"

    tokenizer_ckp = "checkpoints/dtd/tokenizer/tokenizer_type_1.ckp"
    root_path = "U:\\binsearch_data\\train\\dtd"

    tokenizer = pickle.load(open(tokenizer_ckp, 'rb'))
    print("total token: ", len(tokenizer.TokenMap))
    InsTokenizer = InstructionTokenizer()
    file_lst = get_files_ext(root_path=root_path, selected_suf='.pkl')
    # norm_type==0 means keep opcode only
    InsTokenizer.update(file_lst)
    obj = to_py_obj(InsTokenizer)
    pickle.dump(obj, open(tokenizer_ckp, "wb"))
    tokenizer = pickle.load(open(tokenizer_ckp, 'rb'))
    print("total token: ", len(tokenizer.TokenMap))
