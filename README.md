## 如果你懒得看后面
- python bingem_infer_by_dtd.py 运行demo
  - 输入： dtd文件，block模型文件， funciton模型文件
  - 输出：  func_lst, exception_files.
    - func_lst: 一个list，其中每个元素：{'name': 'png_get_oFFs', 'func_id': 5181202862156127406, 'bin_id': '4954968308222105695', 'vec': array(***)}  
    - exception_files: 出错文件 列表
  - 说明： 若一个函数不能产出向量，则不在返回的list中
  
## 原理
- 1 将汇编语言语料看作语料， 训练分词器InstructionTokenizer
  - 训练数据 二进制文件.pkl：包含二进制文件的函数-基本块-指令 信息
- 2 基于InstructionTokenizer, 训练数据为指令序列，pretrain ALM (i.e. BlockEmbedding模型 )
  - 训练数据 二进制文件.pkl：包含二进制文件的函数-基本块-指令 信息
- 3 基于InstructionTokenizer, finetune ALM，至此ALM能够为每个基本块生成embedding
  - 训练数据 block 三元组，prepare_data_pretrain.py 生成成对.hash.dat
- 4 基于ALM和基本块调用图CallGraph，训练ControlGraphEmbedding模型（i.e. 一个图神经网络GraphCNN）
  - 训练数据 function 三元组，使用transferGraph.py 生成（暂时构建neg仅使用同一二进制文件中的不同函数，可优化）
- 5 基于BlockEmbedding和训练ControlGraphEmbedding模型，生成函数向量


## 步骤
- 训练
  - 1  运行 extract_codelines.py ida提取反汇编代码地址信息，保存至 binary.addr.dat
  - 2  运行 batch_extract_dwarf.py，调用extract_blocks.py ida反汇编解析 binary.line.dat， binary.hash.dat
  - 3  运行 pre_for_tokenizer.py 通过 extract_features.py ida反汇编，保存至 binary.pkl
  - 4  运行 instruction_tokenizer.py 更新分词器
  - 5  运行 pre_for_pretrain.py
  - 6  运行 block_embedding_pretrain.py
  - 7  运行 pre_for_finetune.py
  - 8  运行 block_embedding_finetune.py
  - 9  运行 pre_for_func_train.py
  - 10 运行 function_embedding_train.py
- 推理 - 生产函数向量
  - 运行 bingem_infer.py
    - 1 infer_single 给定二进制文件，生成函数向量
    - 2 infer_batch 给定文件夹，生成文件夹下所有二进制文件的函数向量
  - 运行 bingem_infer_dtd.py
    - 1 infer_single 给定单个dtd 函数特征文件，生成函数向量
    - 2 infer_batch 给定文件夹，生成文件夹下所有二进制文件的函数向量
  
## 文件说明   
  - checkpoints 模型存储
  - 模型保存 checkpoints
  - example 示例，包含 1）训练数据database  和 2）模型 checkpoints
  - preprocessing 生成训练数据


## Q&A
- 1 获取 dwarf 信息 
  - 怎么可以得到 二进制代码blk在源码中的行号？任何二进制文件都能通过dwarf能获得吗？信息准确吗？
- 2 tokenizer
  - 记得你说将每个oprnd和每个opcode都看作一个token。但是你的代码token = opcode + "~" + "~".join(oprands)，将一条指令而不是oprnd或opcode当作一个token，对吗？   
- 3 pretrain
  - 训练dataset中，样本是一个blk中的指令序列，而非之前所说func的指令序列
- 4 finetune
  - 三元组的每个元素是一个基本块的embedding ,last_hidden_state[:,-1,:] 最后一层的最后一个token的embedding表示sentence， 而非 之前所说的 倒数第二层


## Requisites
To use TWINS, we need the following tools installed
- IDA Pro 7.5
- Python3.8
- Pytorch
- NearPy
- Networkx
- Numpy
- Scikit-learn
- Transformers 4.1.1
- elftools

## 抽取二进制信息说明
- extract_codelines.py 抽取dwarf_info,生成1个字典对象
  - k:address, v:concat(filename, srcline)， 存于.addr.dat，

- batch_extract_dwarf.py 调用 extract_block.py, 生成2个字典对象
  - 1) k: block的源码hash值, v: blockde反汇编代码 存于hash.dat
  - 2) k: 函数名, v: 所有block的源代码和反汇编代码 存于line.dat

- extract_features.py 生成3个对象
  - 1) 函数控制流md-index， fcg_md_index_class， 存于 .fcg.dat
  - 2）callgraphs, STRINGS_DICT, STRINGS_LIST, imports_map, function_map, func_type_map, func_type_dict 存于.cmp.dat
  - 3) 函数中 所有（以graph为视角的）block信息，包括node, edge, ssp 反汇编代码 存于.pkl
