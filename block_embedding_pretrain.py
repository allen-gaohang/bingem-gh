import torch
from transformers import RobertaConfig, RobertaModel
import os
from transformers import RobertaForMaskedLM
import pickle
from transformers import DataCollatorForLanguageModeling
from transformers import Trainer, TrainingArguments
from instruction_tokenizer import InstructionTokenizer


# tokenizer_path = "checkpoints/luo/tokenizer/tokenizer.model"
# tokenizer_path = "example/checkpoints/tokenizer/tokenizer.model"
# tokenizer_path = "checkpoints/dtd/tokenizer/tokenizer.model"
tokenizer_path = "checkpoints/dtd/tokenizer/tokenizer_type_1.ckp"


# Save path
# block_embedding_path = "checkpoints/luo/block_embedding/pretrain"
# block_embedding_path = "example/checkpoints/block_embedding/pretrain"
block_embedding_path = "checkpoints/dtd/block_embedding/pretrain"


# Assembly sequences
# pretrain_data_path = "database/luo/block_embedding/pretrain_data.pkl"
# pretrain_data_path = "example/database/block_embedding/pretrain_data.pkl"
pretrain_data_path = "database/dtd/block_embedding/pretrain_data.pkl"
pretrain_data_path = "database/dtd/block_embedding/pretrain_data_1.pkl"



epoch = 2
lr = 0
batch_size = 64
grad_acc_steps = 2
warm_up_ratio = 0.8

config = RobertaConfig(
    # vocab_size=32_000,
    # vocab_size=95_000,
    vocab_size=8_000,
    max_position_embeddings=512,
    num_attention_heads=4,
    num_hidden_layers=2,
    hidden_size=64,
    type_vocab_size=1,
)
model = RobertaForMaskedLM(config=config)
device = torch.device('cuda:0')
model.to(device)

InsTokenizer = pickle.load(open(tokenizer_path, "rb"))
datasets = []
if not os.path.exists(pretrain_data_path):
    print("[+] {} not exists".format(pretrain_data_path))
    exit(0)

datasets = pickle.load(open(pretrain_data_path, "rb"))
data_collator = DataCollatorForLanguageModeling(
     tokenizer=InsTokenizer, mlm=True, mlm_probability=0.15
)

warm_up_steps = len(datasets)/(batch_size*grad_acc_steps) * warm_up_ratio
training_args = TrainingArguments(
    output_dir=block_embedding_path +"/model_store",
    overwrite_output_dir=True,
    num_train_epochs=epoch,
    per_device_train_batch_size=batch_size,
    save_steps=2**12,
    save_total_limit=2,
    learning_rate=lr,
    prediction_loss_only=True,
    warmup_steps=warm_up_steps,
    gradient_accumulation_steps=grad_acc_steps
)

trainer = Trainer(
    model=model,
    args=training_args,
    data_collator=data_collator,
    train_dataset=datasets,
)

torch.cuda.empty_cache()
trainer.train()


