# -*-coding:utf-8-*-
import time
import glob
import pickle
import torch
from tqdm import tqdm
import os
from library.model_libs import BlockEmbedding, ControlGraphEmbedding, gen_graphs_by_blk, gen_graphs_by_funcs_attr
import argparse
import numpy as np
import yaml
from instruction_tokenizer import InstructionTokenizer
from my_utils.ida_utils import call_ida
from library.trans_feature import read_func_attr


def gen_emb_by_blk(mdl, input_path, min_blk=5):
    graph_lst = gen_graphs_by_blk(input_path, blk_mdl=mdl, min_blk=min_blk)
    return graph_lst


def gen_emb_by_blk_0(mdl, input_path, min_blk=5):
    try:
        func_attr_lst = read_func_attr(input_path)
    except:
        print("read dtd file error: {}".format(input_path))
    if len(func_attr_lst) > 0:
        graph_lst = gen_graphs_by_funcs_attr(func_attr_lst, blk_mdl=mdl, min_blk=min_blk)
    else:
        graph_lst = []
    return graph_lst, func_attr_lst


def gen_emb_by_func(mdl, inputs=[], min_blk=5):
    name2emb_lst = []
    for i, func in enumerate(inputs):
        func_name = func.label
        name2emb_lst.append({"name": func_name, "vec": None})
        if len(func) < min_blk:
            continue
        try:
            func_features = mdl.generate(func).cpu().numpy()
        except:
            print("gen_emb_by_func for {} error".format(func_name))
            continue
        if True in np.isnan(func_features):
            continue
        name2emb_lst[i]["vec"] = func_features[0]
    return name2emb_lst


def infer_single(block_mdl, function_mdl, file_path, min_blk=5):
    func_lst = []
    try:
        cur = time.time()
        graph_lst, func2attr_lst = gen_emb_by_blk_0(mdl=block_mdl, input_path=file_path, min_blk=min_blk)
        blk_elapsed = time.time() - cur
        cur = time.time()
        if len(graph_lst) > 0:
            func2vec_lst = gen_emb_by_func(mdl=function_mdl, inputs=graph_lst, min_blk=min_blk)
        else:
            print("no functions in dtd")
            exit(1)
        func_elapsed = time.time() - cur
        print("blk:{}, func:{}".format(blk_elapsed, func_elapsed))
        for i in range(len(func2vec_lst)):
            vec = func2vec_lst[i]["vec"]
            if vec is not None:
                name = func2attr_lst[i]["name"]
                func_id = func2attr_lst[i]["func_id"]
                bin_id = func2attr_lst[i]["bin_id"]
                func_lst.append({"name": name, "func_id": func_id, "bin_id": bin_id, "vec": vec})
    except Exception as e:
        print("exception occurs, ignore and continue")
    return func_lst


def infer_batch(block_mdl, function_mdl, dir_path, min_blk=5, begin=None):
    from my_utils.file_utils import get_files_ext
    file_path_lst = get_files_ext(root_path=dir_path, selected_suf=(".json"), begin=begin)
    func_lst = []
    exception_files = []
    for dtd_file_path in tqdm(file_path_lst):
        print(dtd_file_path)
        # cur = time.time()
        graph_lst, func2attr_lst = gen_emb_by_blk_0(mdl=block_mdl, input_path=dtd_file_path, min_blk=min_blk)
        if len(graph_lst) == 0 or len(func2attr_lst) == 0:
            print("found no functions features in dtd")
            continue
        # blk_elapsed = time.time() - cur
        # cur = time.time()
        func2vec_lst = gen_emb_by_func(mdl=function_mdl, inputs=graph_lst, min_blk=min_blk)
        # func_elapsed = time.time() - cur
        # print("blk:{}, func:{}".format(blk_elapsed, func_elapsed))
        for i in range(len(func2vec_lst)):
            vec = func2vec_lst[i].get("vec", None)
            if vec is not None:
                name = func2attr_lst[i].get("name", "no_name")
                func_id = func2attr_lst[i].get("func_id", "no_func_id")
                bin_id = func2attr_lst[i].get("bin_id", "no_bin_id")
                func_lst.append({"name": name, "func_id": func_id, "bin_id": bin_id, "vec": vec})
    return func_lst, exception_files


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='infer_single and infer_batch')
    parser.add_argument('--dir', type=str, default="example\\database\\dtd_for_infer", help='Input directory path ')
    parser.add_argument('--file', type=str,
                        default="U:\\binsearch_data\\funcfeature_compile\\x86_64\\39\\binary\\vlc-0.4.1-win32\\directx.so.tmp0.json",
                        help='Input file path ')
    args = parser.parse_args()
    curDir = os.path.abspath(os.path.dirname(__file__))
    curDir = curDir.replace("\\", "/")
    dir_for_batch = args.dir
    file_for_single = args.file

    use_cuda = True
    device = torch.device('cuda:0' if use_cuda else 'cpu')
    if hasattr(torch.cuda, 'empty_cache'):
        torch.cuda.empty_cache()
    #
    # tokenizer_path = "checkpoints/luo/tokenizer/tokenizer.model"
    # blk_mdl_path = "checkpoints/luo/block_embedding/finetune/best/"
    # fun_mdl_path = "checkpoints/luo/function_embedding/best/function_embedding.ckp"

    tokenizer_path = "checkpoints/dtd/tokenizer/tokenizer_type_0/tokenizer.model"
    blk_mdl_path = "checkpoints/dtd/block_embedding/finetune/best/"
    fun_mdl_path = "checkpoints/dtd/function_embedding/best/function_embedding.ckp"

    blk_mdl = BlockEmbedding(model_directory=blk_mdl_path, tokenizer=tokenizer_path, device=device)

    graph_cnn_config_path = "config/graph_cnn.yaml"
    graph_cnn_config = yaml.load(open(graph_cnn_config_path, "r"), Loader=yaml.FullLoader)
    func_mdl = ControlGraphEmbedding(graph_cnn_config, checkpoint=fun_mdl_path, device=device)

    ## test infer_single
    # cur = time.time()
    # func_lst = infer_single(block_mdl=blk_mdl, function_mdl=func_mdl, file_path=file_for_single, min_blk=5)
    # elapsed = time.time() - cur
    # for f in func_lst:
    #     print(f)
    # print("n_func:{}, infer time: {}s".format(len(func_lst), elapsed))

    ## test infer_batch
    # dtd_dir_path = "U:/binsearch_data/funcfeature_compile/x86_64/975"
    # dtd_dir_path = "U:/binsearch_data/funcfeature_compile/x86_64/39"
    dtd_dir_path = "D:/gaohang/workspace/bingem-gh/example/database/dtd_for_infer"
    # dtd_dir_path = "U:\\binsearch_data\\funcfeature_compile\\x86_64\\5\\binary\\libgsoap-2.8.104-dbgsym_2.8.104-3_alpha"
    # begin = "U:\\binsearch_data\\funcfeature_compile\\x86_64\\39\\binary\\vlc-0.4.1-win32\\directx.so.tmp0.json"
    function_lst, exception_files = infer_batch(block_mdl=blk_mdl, function_mdl=func_mdl, dir_path=dtd_dir_path,
                                                min_blk=2)
    print("Total functions: {}, exception_files: {}".format(len(function_lst), len(exception_files)))
