# -*-coding:utf-8-*-
import pickle
import networkx as nx
import torch
from transformers import RobertaModel
import torch.utils.data as Data
import yaml
from library.gcn.graphcnn import GraphCNN

cs_tag = "tagcs"
loc_tag = "tagloc"
locret_tag = "taglocret"
ptr_tag = "tagptr"


class S2VGraph(object):
    def __init__(self, g, label, node_tags=None, node_features=None):
        '''
            g: a networkx graph
            label: an integer graph label
            node_tags: a list of integer node tags
            node_features: a torch float tensor, one-hot representation of the tag that is used as input to neural nets
            edge_mat: a torch long tensor, contain edge list, will be used to create torch sparse tensor
            neighbors: list of neighbors (without self-loop)
        '''
        self.label = label
        self.g = g
        self.node_tags = node_tags
        self.neighbors = []
        self.node_features = []
        self.edge_mat = 0

        self.max_neighbor = 0
        self.binary_name = ""
        self.constants = []
        self.size = 0
        self.nodes = 0
        self.edges = 0
        self.instructions = 0

    def __len__(self):
        """Returns the number of nodes in the graph. Use: 'len(G)'.

        Returns
        -------
        nnodes : int
            The number of nodes in the graph.

        See Also
        --------
        """
        return len(self.g._node)


class BlockEmbedding:
    def __init__(self,
                 model_directory=None,
                 opr_list=None,
                 tokenizer=None,
                 device=torch.device('cuda:0'),
                 batch_size=64,
                 do_float16=True
                 ):
        self.tokenizer = pickle.load(open(tokenizer, "rb"))
        self.model = RobertaModel.from_pretrained(model_directory, add_pooling_layer=True)
        self.device = device
        self.model.to(self.device)
        self.model.eval()
        self.opr_list = None
        self.batch_size = batch_size
        if do_float16:
            self.model.half()
        if opr_list != None:
            self.opr_list = opr_list

    def batch_generate(self, ins_seq_list):
        inputs = self.tokenizer.pad(ins_seq_list, pad_length=256)['input_ids']
        data_loader = Data.DataLoader(inputs, batch_size=self.batch_size)
        features = []
        for batch_input in data_loader:
            batch_input = batch_input.to(self.device)
            output = self.model(batch_input)
            features.extend(list(output.last_hidden_state[:, -1, :].detach()))
        return features

    def read_blk(self, instructions: list):
        tokens = self.tokenizer(instructions, return_tensors="pt", max_length=254, padding=True, truncation=True)
        return tokens


class ControlGraphEmbedding:
    def __init__(self, graph_cnn_config,
                 device=torch.device('cuda:0'),
                 checkpoint=None):
        num_layers = graph_cnn_config['num_layers']
        num_mlp_layers = graph_cnn_config['num_mlp_layers']
        input_dim = graph_cnn_config['input_dim']
        hidden_dim = graph_cnn_config['hidden_dim']
        output_dim = graph_cnn_config['output_dim']
        neighbor_pooling_type = graph_cnn_config['neighbor_pooling_type']
        graph_pooling_type = graph_cnn_config['graph_pooling_type']
        final_dropout = graph_cnn_config['final_dropout']
        learn_eps = graph_cnn_config['learn_eps']
        self.model = GraphCNN(num_layers, num_mlp_layers, input_dim, hidden_dim, output_dim, final_dropout, learn_eps, graph_pooling_type, neighbor_pooling_type, device)
        self.model.load_state_dict(torch.load(checkpoint))
        self.model.eval()

    def generate(self, graph):
        output = self.model(graph)
        output = output.detach()
        return output


def generate_graph(graph, blkAttrs, label=0):
    g = S2VGraph(graph, label)
    g.neighbors = [[] for _ in range(len(g.g))]
    for i, j in g.g.edges():
        g.neighbors[i].append(j)
        g.neighbors[j].append(i)
    tensor_lst = []
    for n, nodeAttr in zip(range(len(blkAttrs)), blkAttrs):
        tensor_lst.append(nodeAttr.unsqueeze(0))
    g.node_features = torch.cat(tensor_lst, 0)
    # g.node_features = torch.empty(len(g.g), 128)
    # for n, nodeAttr in zip(range(len(blkAttrs)), blkAttrs):
    #     g.node_features[n] = nodeAttr
    edges = [list(pair) for pair in g.g.edges()]
    if edges:
        g.edge_mat = torch.LongTensor(edges).transpose(0, 1)
    else:
        g.edge_mat = []
    return g


def read_pickle(filename, blk_mdl, min_blk=5):
    Feat = pickle.load(open(filename, "rb"))
    Func_dict = {}
    for func in Feat:
        g = nx.Graph()
        edges = []
        blkInsSeq = []
        if len(func['blocks']) < min_blk:
            continue
        for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
            g.add_node(cur)
            blkInsSeq.append(blk_mdl.read_blk(func['blocks'][cur]['src']))
            for nextblk in blk['succs']:
                edges.append([cur, nextblk])
        blkAttrs = blk_mdl.batch_generate(blkInsSeq)
        g.add_edges_from(edges)
        func_graph = generate_graph(g, blkAttrs, func['name'])
        func_graph.binary_name = func["filename"]
        func_graph.constants = func['constants']
        func_graph.size = func['size']
        func_graph.nodes = func['nodes']
        func_graph.edges = func['edges']
        func_graph.instructions = func['instructions']
        Func_dict[func['name']] = func_graph
    return Func_dict


def gen_graphs_by_blk(filename, blk_mdl, min_blk=5):
    funcs_attr = pickle.load(open(filename, "rb"))
    g_lst = []
    for func in funcs_attr:
        g = nx.Graph()
        edges = []
        blkInsSeq = []
        if len(func['blocks']) < min_blk:
            continue
        for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
            g.add_node(cur)
            blkInsSeq.append(blk_mdl.read_blk(func['blocks'][cur]['src']))
            for nextblk in blk['succs']:
                edges.append([cur, nextblk])
        blks_emb = blk_mdl.batch_generate(blkInsSeq)
        g.add_edges_from(edges)
        func_graph = generate_graph(g, blks_emb, func['name'])
        func_graph.binary_name = func["filename"]
        func_graph.constants = func['constants']
        func_graph.size = func['size']
        func_graph.nodes = func['nodes']
        func_graph.edges = func['edges']
        func_graph.instructions = func['instructions']
        g_lst.append(func_graph)
    return g_lst


def gen_graphs_by_funcs_attr(funcs_attr, blk_mdl, min_blk=5):
    g_lst = []
    for func in funcs_attr:
        g = nx.Graph()
        edges = []
        blkInsSeq = []
        if len(func.get('blocks', 0)) < min_blk:
            continue
        for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
            g.add_node(cur)
            disassam_code = func['blocks'][cur].get("src", [])
            blkInsSeq.append(blk_mdl.read_blk(disassam_code))
            for nextblk in blk.get('succs', []):
                edges.append([cur, nextblk])
        blks_emb = blk_mdl.batch_generate(blkInsSeq)
        g.add_edges_from(edges)
        func_graph = generate_graph(g, blks_emb, func.get('name', None))
        func_graph.binary_name = func.get("filename", None)
        func_graph.constants = func.get('constants', None)
        func_graph.size = func.get('size', None)
        func_graph.nodes = func.get('nodes', None)
        func_graph.edges = func.get('edges', None)
        func_graph.instructions = func.get('instructions', None)
        g_lst.append(func_graph)
    return g_lst


def gen_graphs_by_funcs_attr(funcs_attr, blk_mdl, min_blk=5):
    g_lst = []
    for func in funcs_attr:
        try:
            g = nx.Graph()
            edges = []
            blkInsSeq = []
            if len(func.get('blocks', 0)) < min_blk:
                continue
            for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
                g.add_node(cur)
                disassam_code = func['blocks'][cur].get("src", [])
                blkInsSeq.append(blk_mdl.read_blk(disassam_code))
                for nextblk in blk.get('succs', []):
                    edges.append([cur, nextblk])
            blks_emb = blk_mdl.batch_generate(blkInsSeq)
            g.add_edges_from(edges)
            func_graph = generate_graph(g, blks_emb, func.get('name', None))
            func_graph.binary_name = func.get("filename", None)
            func_graph.constants = func.get('constants', None)
            func_graph.size = func.get('size', None)
            func_graph.nodes = func.get('nodes', None)
            func_graph.edges = func.get('edges', None)
            func_graph.instructions = func.get('instructions', None)
            g_lst.append(func_graph)
        except:
            print("gen_blk_embedding for {} error".format(func.get('name', "no name")))
            continue
    return g_lst


def read_pickle_gen_special_functions(filename, BlkEmbedding=None, function_list=[], dim=128, min_blk=5):
    Feat = pickle.load(open(filename, "rb"))
    Func_dict = {}
    for func in Feat:
        g = nx.Graph()
        edges = []
        blkInsSeq = []
        if func['name'] not in function_list:
            continue
        if len(func['blocks']) < min_blk:
            continue
        for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
            g.add_node(cur)
            blkInsSeq.append(BlkEmbedding.read_blk(func['blocks'][cur]['src']))
            for nextblk in blk['succs']:
                edges.append([cur, nextblk])
        blkAttrs = BlkEmbedding.batch_generate(blkInsSeq)

        g.add_edges_from(edges)
        func_graph = generate_graph(g, blkAttrs, func['name'], dim=dim)

        func_graph.binary_name = func["filename"]
        func_graph.constants = func['constants']
        func_graph.size = func['size']
        func_graph.nodes = func['nodes']
        func_graph.edges = func['edges']
        func_graph.instructions = func['instructions']

        Func_dict[func['name']] = func_graph

    return Func_dict


def read_data_gen_one_list(input_files=[], model_directory="./block_embedding", device=torch.device('cuda:0')):
    BlkEmbedding = BlockEmbedding(model_directory=model_directory, device=device)
    data = []
    for fname in input_files:
        func_dict = read_pickle(fname, blk_mdl=BlkEmbedding)
        data.extend(list(func_dict.values()))
    return data


if __name__ == '__main__':
    model_directory = "D:/gaohang/workspace/bingem-gh/checkpoints/block_embedding/best"
    model = RobertaModel.from_pretrained(model_directory, add_pooling_layer=True)
    print(model)