import json
from my_utils import id_generator as idg


def read_func_attr(path):
    with open(path, 'r', encoding='utf-8') as f_func:
        ln_file = f_func.readline()
        dct_file = json.loads(ln_file)
        func_attrs = []
        bin_id = str(dct_file.get('id', None))
        functions = dct_file.get("Functions", [])
        for func_dct in functions:
            func_seg = func_dct.get("segment", None)
            if func_seg != ".text":
                continue
            blk_infos = []
            sea = str(func_dct.get('sea', None))
            sHash = str(func_dct.get('sHash', None))
            func_id = idg.construct_id([bin_id, sea, sHash])
            func_name = func_dct.get("name", None)
            func_dct_blocks = func_dct.get('blocks', [])
            for blk in func_dct_blocks:
                diassam_ins = []
                for s in blk['src']:
                    diassam_ins.append(s[1:])
                blk_info = {'id': blk.get('id', None), 'bytes': '', 'src': diassam_ins, 'oprType': [], 'dat': {},
                            'succs': [int(x) for x in blk['calls']]}
                blk_infos.append(blk_info)
            f_attr = {'filename': path, 'name': func_name, 'blocks': blk_infos, "func_id": func_id, "bin_id": bin_id,
                      'nodes': func_dct.get('node_size', None), 'edges': func_dct.get('edge_size', None),
                      'instructions': func_dct.get('inst_size', None), 'constants': [], 'size': 0, }
            # 'sea': sea, 'sHash': sHash, 'filename': '', 'mnemonics_spp': 0}
            func_attrs.append(f_attr)
    return func_attrs


if __name__ == '__main__':
    path_func = "U:\\binsearch_data\\funcfeature_compile\\x86_64\\5\\binary\\libgsoap-2.8.104-dbgsym_2.8.104-3_alpha"
    from my_utils.file_utils import get_files_ext
    paths = get_files_ext(root_path=path_func, selected_suf=".json")
    for p in paths:
        func_attr = read_func_attr(p)
        print(func_attr)
