# -*-coding:utf-8-*-
import time
import yaml
import pickle
import torch
from tqdm import tqdm
import os
import subprocess
from library.model_libs import read_pickle, BlockEmbedding, ControlGraphEmbedding
import argparse
import numpy as np
from library.MatchingScheme import Markov_LSH_Matching

def gen_one_list_for_each_binary(input_files=[], min_blk=5):

    global BlkEmbedding
    data = []
    for fname in input_files:
        # pkl file contains disassamble code text
        func_dict = read_pickle(fname, blk_mdl=BlkEmbedding, dim=128, min_blk=min_blk)
        data.extend(list(func_dict.values()))
    return data


def build_for_one_list(inputs=[], min_blk=5):
    FunctionName = []
    VectorTable = []
    DetailData = {}
    FunctionMap = {}
    id = 0
    global CfgEmbedding

    pbar = tqdm(inputs)
    for func in pbar:
        funcName = func.label
        if len(func) < min_blk:
            continue

        func_features = CfgEmbedding.generate(func).cpu().numpy()
        if True in np.isnan(func_features):
            continue
        FunctionName.append((id, funcName))
        DetailData[id] = {"binary_name": func.binary_name,
                          "constants": func.constants,
                          "funcname": funcName
                          }
        FunctionMap[funcName] = id
        id += 1
        VectorTable.append(func_features[0])

    return FunctionName, VectorTable, FunctionMap


def get_idb_file(binary_name):
    if not os.path.exists(binary_name):
        print("[!] {} does not exist.".format(binary_name))
        exit(-1)
    print("[+] Extracting Binary Features")
    cmd_str = '"{}" -A -S"{}" "{}"'.format(cmd64, script, binary_name)
    print(cmd_str)
    res = subprocess.Popen(cmd_str, shell=True, stdin=subprocess.PIPE, stdout=subprocess.PIPE,
                           stderr=subprocess.PIPE)
    result = res.stdout.read()
    res.wait()
    res.stdout.close()
    print("cmd output: ", result)
    while True:
        if os.path.exists(binary_name+".pkl"):
            break
        else:
            time.sleep(3)
    print("[+] Extraction is complete!")


def handle_binary(binary_name, min_blk=5):
    input_name = binary_name + ".pkl"
    file_dict = gen_one_list_for_each_binary(input_files=[input_name], min_blk=min_blk)
    FunctionName, VectorTable, FunctionMap = build_for_one_list(file_dict, min_blk=min_blk)

    return FunctionName, VectorTable, FunctionMap


def compare_binaries_with_sequences(bin1, bin2):
    FN1, VT1, FM1 = handle_binary(bin1)
    FN2, VT2, FM2 = handle_binary(bin2)
    return Markov_LSH_Matching(bin1, bin2, (FN1, VT1, FM1), (FN2, VT2, FM2))


if __name__ == '__main__':

    use_cuda = True
    device = torch.device('cuda:0' if use_cuda else 'cpu')
    if hasattr(torch.cuda, 'empty_cache'):
        torch.cuda.empty_cache()

    # ida64_absolute_path
    ida_config_path = "./config/ida.yaml"
    ida_config = yaml.load(open(ida_config_path), Loader=yaml.FullLoader)
    cmd64 = ida_config['cmd64']

    # script_absolute_path
    script = "extract_features/extract_features.py"

    ckpt = "function_embedding/best/model.ckpt"

    CFGconfig = "config/config.args"
    tokenizer_path = "checkpoints/tokenizer/tokenizer.model"
    blkEmbedding = "block_embedding/best"
    dimension = 128

    parser = argparse.ArgumentParser(
        description='Compare two binaries.')
    parser.add_argument('--file1', type=str, default="./example/b2sum_O3_Coreutils",
                        help='Input File 1')
    parser.add_argument('--file2', type=str, default="./example/b2sum_O2_Coreutils",
                        help='Input File 2')
    args = parser.parse_args()
    file1 = args.file1
    file2 = args.file2
    # get_idb_file(file1)
    # get_idb_file(file2)

    BlkEmbedding = BlockEmbedding(model_directory=blkEmbedding, tokenizer=tokenizer_path, device=device)
    if CFGconfig and ckpt:
        CGEconfig = pickle.load(open(CFGconfig, "rb"))
    else:
        print("[!] Error")
        exit(-1)

    CfgEmbedding = ControlGraphEmbedding(args=CGEconfig, checkpoint=ckpt)

    compare_binaries_with_sequences(file1, file2)
