# -*-coding:utf-8-*-
import os
import time
import yaml
import torch
from tqdm import tqdm
import argparse
import numpy as np
from library.gcn.graphcnn import GraphCNN
from library.model_libs import BlockEmbedding, ControlGraphEmbedding, gen_graphs_by_blk
from instruction_tokenizer import InstructionTokenizer
from my_utils.ida_utils import call_ida


def gen_emb_by_blk(mdl, input_path, min_blk=5):
    graph_lst = gen_graphs_by_blk(input_path, blk_mdl=mdl, min_blk=min_blk)
    return graph_lst


def gen_emb_by_func(mdl, inputs=[], min_blk=5):
    func_emb_map = {}
    pbar = tqdm(inputs)
    for func in pbar:
        func_name = func.label
        if len(func) < min_blk:
            continue
        func_features = mdl.generate(func).cpu().numpy()
        if True in np.isnan(func_features):
            continue
        func_emb_map[func_name] = func_features[0]
    return func_emb_map


def infer(block_mdl, function_mdl, input_path, min_blk=5):
    cur = time.time()
    graph_lst = gen_emb_by_blk(mdl=block_mdl, input_path=input_path, min_blk=min_blk)
    blk_elapsed = time.time() - cur
    cur = time.time()
    func2vec_dct = gen_emb_by_func(mdl=function_mdl, inputs=graph_lst, min_blk=min_blk)
    func_elapsed = time.time() - cur
    print("blk:{}, func:{}".format(blk_elapsed,func_elapsed))
    return func2vec_dct


if __name__ == '__main__':
    # tokenizer_path = "example/checkpoints/tokenizer/tokenizer.model"
    # blk_path = "example/checkpoints/block_embedding/best/"
    # fun_mdl_path = "example/checkpoints/function_embedding/best/function_embedding.ckp"

    tokenizer_path = "checkpoints/luo/tokenizer/tokenizer.model"
    blk_path = "checkpoints/luo/block_embedding/pretrain/best/"
    fun_mdl_path = "checkpoints/luo/function_embedding/best/function_embedding.ckp"

    torch.backends.cudnn.enabled = False
    CUDA_LAUNCH_BLOCKING = 1
    parser = argparse.ArgumentParser(
        description='infer, input: binary file, output: functions embedding')
    parser.add_argument('--file', type=str, default="example/database/binaries/srun",
                        help='Input File ')
    args = parser.parse_args()
    file = args.file
    use_cuda = True
    device = torch.device('cuda:0' if use_cuda else 'cpu')
    if hasattr(torch.cuda, 'empty_cache'):
        torch.cuda.empty_cache()

    tokenizer_path = "checkpoints/dtd/tokenizer/tokenizer_type_0/tokenizer.model"
    blk_mdl_path = "checkpoints/dtd/block_embedding/finetune/best/"
    fun_mdl_path = "checkpoints/dtd/function_embedding/best/function_embedding.ckp"

    blk_mdl = BlockEmbedding(model_directory=blk_mdl_path, tokenizer=tokenizer_path, device=device)

    graph_cnn_config_path = "config/graph_cnn.yaml"
    graph_cnn_config = yaml.load(open(graph_cnn_config_path, "r"), Loader=yaml.FullLoader)
    func_mdl = ControlGraphEmbedding(graph_cnn_config, checkpoint=fun_mdl_path, device=device)

    curDir = os.path.abspath(os.path.dirname(__file__))
    curDir = curDir.replace("\\", "/")
    ida_config_path = "config/ida.yaml"
    ida_config = yaml.load(open(ida_config_path), Loader=yaml.FullLoader)
    cmd64 = ida_config['cmd64']
    script = curDir + "/preprocessing/extract_features.py"
    cur = time.time()
    call_ida(ida_abs_path=cmd64, py_path=script, bin_paths=[file])
    elapsed = time.time() - cur
    print("get_idb time: {}s".format(elapsed))

    cur = time.time()
    func2vec_dct = infer(block_mdl=blk_mdl, function_mdl=func_mdl, input_path=file + '.pkl', min_blk=5)
    elapsed = time.time() - cur
    print("n_func:{0}, infer time: {1}s".format(len(func2vec_dct), elapsed))
