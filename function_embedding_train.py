# -*-coding:utf-8-*-
import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
import yaml
import numpy as np
from tqdm import tqdm
from sklearn.model_selection import train_test_split
import pickle
from pathlib import Path
from library.model_libs import S2VGraph, BlockEmbedding
from instruction_tokenizer import InstructionTokenizer
from library.gcn.graphcnn import GraphCNN

P_norm = 2
triplet_criterion = nn.TripletMarginLoss(margin=20, p=P_norm)
ce_criterion = nn.CrossEntropyLoss()
# criterion_mse = nn.MSELoss()

def read_data(file_path=None):
    with open(file_path, "rb") as f:
        data = pickle.load(f)
    return data


def train(model, train_graphs, optimizer, batch_size, epoch, iters_per_epoch):
    model.train()
    total_iters = iters_per_epoch
    pbar = tqdm(range(total_iters), unit='batch')
    loss_accum = 0
    Lambda = 0.5
    for pos in pbar:
        selected_idx = np.random.permutation(len(train_graphs))[:batch_size]
        batch_graph = [train_graphs[idx] for idx in selected_idx]
        anchor_inputs, pos_inputs, neg_inputs = [], [], []
        for i,_ in enumerate(batch_graph):
            anchor_inputs.append(_[0])
            pos_inputs.append(_[1])
            neg_inputs.append(_[2])
        anchor_outputs = model(anchor_inputs).to(device=torch.device(model.device))
        pos_outputs = model(pos_inputs).to(device=torch.device(model.device))
        neg_outputs = model(neg_inputs).to(device=torch.device(model.device))
        pos_sim = 1 / (1 + torch.cdist(anchor_outputs, pos_outputs).diag())
        neg_sim = 1 / (1 + torch.cdist(anchor_outputs, neg_outputs).diag())
        p = torch.stack([1 - pos_sim, pos_sim], dim=-1)
        n = torch.stack([1 - neg_sim, neg_sim], dim=-1)
        preds = torch.cat((p, n), dim=0).to(device=torch.device(model.device))
        labels = torch.tensor([1] * len(pos_sim) + [0] * len(neg_sim))
        labels = labels.to(device=torch.device(model.device))
        loss_tri = triplet_criterion(anchor_outputs, pos_outputs, neg_outputs)
        loss_ce = ce_criterion(preds, labels)
        # loss_mse = criterion_mse(anchor_outputs, pos_outputs)
        loss = loss_tri + Lambda * loss_ce

        # backprop
        if optimizer is not None:
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()

        loss = loss.detach().cpu().numpy()
        loss_accum += loss

        # report
        pbar.set_description('epoch: %d' % (epoch))

    average_loss = loss_accum / total_iters
    print("loss training: %f" % (average_loss))

    return average_loss


def test(model, input_graphs, batch_size):
    model.eval()

    selected_idx = np.random.permutation(len(input_graphs))[:batch_size]
    test_graphs = [input_graphs[idx] for idx in selected_idx]
    output = pass_data_iteratively(model, test_graphs)

    Pred = torch.cat(
        [torch.pairwise_distance(i, k, p=P_norm) - torch.pairwise_distance(i, j, p=P_norm) for i, j, k in output],
        0).to('cpu')
    all_case = len(Pred)

    TP = len(Pred[torch.where(Pred > 0)])
    acc = TP / all_case
    distance_avg = Pred.sum() / all_case
    print("Distance", distance_avg)
    print("Accuracy", acc)

    return distance_avg.cpu().numpy(), acc


###pass data to model with minibatch during testing to avoid memory overflow (does not perform backpropagation)
def pass_data_iteratively(model, graphs, minibatch_size=256):
    model.eval()
    output = []
    idx = np.arange(len(graphs))
    for i in range(0, len(graphs), minibatch_size):
        sampled_idx = idx[i:i + minibatch_size]
        if len(sampled_idx) == 0:
            continue

        batch_graph = [graphs[j] for j in sampled_idx]
        anchor_graph = [_[0] for _ in batch_graph]
        positive_graph = [_[1] for _ in batch_graph]
        negative_graph = [_[2] for _ in batch_graph]
        anchor_output = model(anchor_graph).detach()
        positive_output = model(positive_graph).detach()
        negative_output = model(negative_graph).detach()

        output.append((anchor_output, positive_output, negative_output))
    return output


# Modified below code:
# https://github.com/weihua916/powerful-gnns
def main():
    # Training settings
    parser = argparse.ArgumentParser(
        description='PyTorch graph convolutional neural net for function similarity classification')
    parser.add_argument('--device', type=int, default=0,
                        help='which gpu to use if any (default: 0)')
    parser.add_argument('--batch_size', type=int, default=256,
                        help='input batch size for training (default: 256)')
    parser.add_argument('--epochs', type=int, default=32,
                        help='number of epochs to train (default: 350)')
    parser.add_argument('--lr', type=float, default=1e-3,
                        help='learning rate (default: 0.01)')
    parser.add_argument('--seed', type=int, default=0,
                        help='random seed for splitting the dataset into 10 (default: 0)')

    args = parser.parse_args()
    batch_size = args.batch_size
    lr = args.lr
    epochs = args.epochs
    device_num = str(args.device)

    data_dir = "database/dtd/function_embedding/function_embedding_data.pkl"
    model_save_dir = "checkpoints/dtd/function_embedding/"
    device = torch.device("cuda:" + device_num) if torch.cuda.is_available() else torch.device("cpu")

    graphs = read_data(file_path=data_dir)
    train_graphs, test_graphs = train_test_split(graphs, test_size=0.1, random_state=66, shuffle=True)
    print("Train:", len(train_graphs), "\tTest:", len(test_graphs))

    graph_cnn_config_path = "config/graph_cnn.yaml"
    graph_cnn_config = yaml.load(open(graph_cnn_config_path, "r"), Loader=yaml.FullLoader)

    model = GraphCNN(graph_cnn_config.get("num_layers"),
                     graph_cnn_config.get("num_mlp_layers"),
                     train_graphs[0][0].node_features.shape[1],
                     graph_cnn_config.get("hidden_dim"),
                     graph_cnn_config.get("output_dim"),
                     graph_cnn_config.get("final_dropout"),
                     graph_cnn_config.get("learn_eps"),
                     graph_cnn_config.get("graph_pooling_type"),
                     graph_cnn_config.get("neighbor_pooling_type"),
                     device)

    optimizer = optim.Adam(model.parameters(), lr=lr)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=10, gamma=0.95)

    iters_per_epoch = int(len(train_graphs) / batch_size)
    best_results = 0.8
    for ith_epoch in range(1, epochs + 1):
        avg_loss = train(model, train_graphs, optimizer, batch_size, ith_epoch, iters_per_epoch)
        scheduler.step()
        distance_test, test_acc = test(model, test_graphs, batch_size)
        if test_acc > best_results:
            torch.save(model.state_dict(),
                       model_save_dir + "{}_{}.ckp".format(test_acc, distance_test))


if __name__ == '__main__':
    main()
