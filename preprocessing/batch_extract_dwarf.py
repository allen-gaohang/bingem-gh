# -*-coding:utf-8-*-
import os
import sys
sys.path.append("..")
from pathlib import Path
import zipfile
import yaml
from my_utils.file_utils import get_files_ext
from my_utils.ida_utils import call_ida


def unzip(file_name):
    extracted_path = file_name.replace(".zip", "")
    zip_file = zipfile.ZipFile(file_name)
    zip_file.extractall(extracted_path)
    zip_file.close()
    return [str(x) for x in Path(extracted_path).glob("**/*")]


def main():
    curDir = os.path.abspath(os.path.dirname(__file__))
    curDir = curDir.replace("\\", "/")
    ida_config_path = "../config/ida.yaml"
    ida_config = yaml.load(open(ida_config_path), Loader=yaml.FullLoader)
    cmd64 = ida_config['cmd64']
    script = curDir + "/extract_blocks.py"
    filter = (".dat", ".pkl", ".i64", ".idb", ".txt", ".json", ".til", ".nam", ".id0", ".id1", ".id2")
    # bins_root = "..\\example\\database\\binaries"
    bins_root = ["U:\\binsearch_data\\train\\dtd\\4", "U:\\binsearch_data\\train\\dtd\\5"]
    bin_lst = get_files_ext(root_path=bins_root, filtered_suf=filter)
    call_ida(ida_abs_path=cmd64, py_path=script, bin_paths=bin_lst)


if __name__ == "__main__":
    main()
