# -*-coding:utf-8-*-
import os.path
import sys
import random

sys.path.append("..")
from pathlib import Path
from instruction_tokenizer import InstructionTokenizer
import pickle
import numpy as np
import time


def load_files(f):
    return pickle.load(open(f, "rb"))


def generate_triplet(hdict1, hdict2, seed=666, tokenizer=None, input_data=None):
    data_triplet = []
    keys = list(hdict2.keys())
    randGen = np.random.RandomState(seed)

    hdict2_tokened = {}
    for hashnum in hdict2:
        hdict2_tokened[hashnum] = tokenizer(hdict2[hashnum], truncation=True)

    max_upper = len(keys) - 20
    if max_upper<0:
        return None
    for hashnum in hdict1:
        t1 = time.time()
        if not hashnum:
            continue
        if hashnum in hdict2:
            anchor = hdict1[hashnum]
            anchor = tokenizer(anchor, truncation=True)
            positive = hdict2_tokened[hashnum]
            negNumber = randGen.randint(max_upper)
            for _ in range(negNumber, negNumber + 10):
                negSample = keys[_]

                if negSample == hashnum:
                    continue

                negative = hdict2_tokened[negSample]
                data_triplet.append([anchor, positive, negative])

    if input_data:
        data_triplet.extend(input_data)
    return data_triplet


def gen_by_lst_dirs(root_path="../database/finetune", finetune_data_path="../database/finetune_data.pkl"):
    tokenizer = pickle.load(open("../checkpoints/tokenizer/tokenizer.model", "rb"))
    all_triplet_blocks = []
    for d in Path(root_path).glob("*"):
        if not os.path.isdir(d):
            continue
        file_pair = os.listdir(d)
        file_pair.sort()
        print(file_pair)
        triplet_blocks = generate_triplet(load_files(file_pair[0]), load_files(file_pair[1]), tokenizer=tokenizer)
        all_triplet_blocks.extend(triplet_blocks)
    pickle.dump(all_triplet_blocks, open(finetune_data_path, "wb"))


def gen_triplet_by_pair_files(file1, file2, tokenizer):
    triplet_blocks = generate_triplet(load_files(file1), load_files(file2), tokenizer=tokenizer)
    return triplet_blocks


def prepare_hash_dat_files(old_root="U:\\binsearch_data\\train\\luo\\gcc",
                           new_root="U:\\binsearch_data\\train\\luo\\finetune"):
    import shutil
    from my_utils.file_utils import get_files_ext, modify_path_by_dir_concat
    all_files = get_files_ext(root_path=old_root, selected_suf=".hash.dat")
    for old_path in all_files:
        new_path = modify_path_by_dir_concat(path=old_path, new_root=new_root)
        print("{}  ->  {}".format(old_path, new_path))
        os.makedirs(os.path.dirname(new_path), exist_ok=True)
        shutil.copy(old_path, new_path)


def gen_path_by_chng_compiler(path):
    comp_opts = ["clang", "gcc"]
    path_segs = path.split("\\")
    for i, dir in enumerate(path_segs):
        if dir in comp_opts:
            p_i = i
            p_dir = random.choice(comp_opts)
            while p_dir == dir:
                p_dir = random.choice(comp_opts)
    path_segs[p_i] = p_dir
    new_path = "\\".join(path_segs)
    if not os.path.exists(new_path):
        new_path = None
    return new_path


def gen_path_by_chng_optm(path):
    optm_opts = ["O0", "O1", "O2", "O3", ]
    path_segs = path.split("\\")
    for i, dir in enumerate(path_segs):
        if dir in optm_opts:
            p_i = i
            p_dir = random.choice(optm_opts)
            while p_dir == dir:
                p_dir = random.choice(optm_opts)
    path_segs[p_i] = p_dir
    new_path = "\\".join(path_segs)
    if not os.path.exists(new_path):
        new_path = None
    return new_path


def prepare_data_train_example(tokenizer_path="../example/checkpoints/tokenizer/tokenizer.model",
                               save_path="../example/database/block_embedding/finetune_data.pkl"):
    file1 = "../example/database/binaries/b2sum_O2_Coreutils.hash.dat"
    file2 = "../example/database/binaries/b2sum_O3_Coreutils.hash.dat"
    tokenizer = pickle.load(open(tokenizer_path, "rb"))
    triplet_blocks = generate_triplet(load_files(file1), load_files(file2), tokenizer=tokenizer)
    pickle.dump(triplet_blocks[:4096], open(save_path, "wb"))
    print("Total triplet blocks: {}, saved in {}".format(len(triplet_blocks), saved_path))


def prepare_train_0(n_anchor, tokenizer_path, src_path, save_path):
    error = 0
    all_triplet_blocks = []
    tokenizer = pickle.load(open(tokenizer_path, "rb"))
    from my_utils.file_utils import get_files_ext
    path_lst = get_files_ext(root_path=src_path, selected_suf=".hash.dat")
    from random import sample
    if n_anchor == 0:
        n_anchor = len(path_lst) / 2
    base_files = sample(path_lst, int(n_anchor))
    for file1 in base_files:
        file2 = gen_path_by_chng_optm(file1)
        if file1 and file2:
            t = generate_triplet(load_files(file1), load_files(file2), tokenizer=tokenizer)
            if t:
                all_triplet_blocks.extend(t)
                continue
        error += 1
    pickle.dump(all_triplet_blocks, open(save_path, "wb"))
    print("Total triplet blocks: {}, saved in {}.".format(len(all_triplet_blocks), saved_path))
    print("Total anchor: {}, error:{}".format(n_anchor, error))


def prepare_train_1(train_dir, tokenizer_path, save_path, n_anchor=64):
    n_error = 0
    tokenizer = pickle.load(open(tokenizer_path, "rb"))
    from my_utils.file_utils import get_subpath_ext
    _, all_O0_files = get_subpath_ext(root_path=train_dir, search_type="**/*", selected_suf=".pkl", only_path="O0")
    _, all_O1_files = get_subpath_ext(root_path=train_dir, search_type="**/*", selected_suf=".pkl", only_path="O1")
    _, all_O2_files = get_subpath_ext(root_path=train_dir, search_type="**/*", selected_suf=".pkl", only_path="O2")
    _, all_O3_files = get_subpath_ext(root_path=train_dir, search_type="**/*", selected_suf=".pkl", only_path="O3")
    anchor_names = [os.path.basename(x) for x in all_O0_files]
    name2path = {}
    name2path["O0"] = dict(zip([os.path.basename(x) for x in all_O0_files], all_O0_files))
    name2path["O1"] = dict(zip([os.path.basename(x) for x in all_O1_files], all_O1_files))
    name2path["O2"] = dict(zip([os.path.basename(x) for x in all_O2_files], all_O2_files))
    name2path["O3"] = dict(zip([os.path.basename(x) for x in all_O3_files], all_O3_files))
    all_triplet_blocks = []
    for anchor_name in random.choices(anchor_names, k=n_anchor):
        if anchor_name in name2path["O0"].keys() and anchor_name in name2path["O3"].keys():
            t = generate_triplet(load_files(name2path["O0"][anchor_name]), load_files(name2path["O3"][anchor_name]), tokenizer=tokenizer)
            all_triplet_blocks.extend(t)
        else:
            n_error =+ 1
    pickle.dump(all_triplet_blocks, open(save_path, "wb"))
    print("Total triplet blocks: {}, saved in {}.".format(len(all_triplet_blocks), save_path))
    print("Total anchor: {}, error:{}".format(n_anchor, n_error))


if __name__ == '__main__':
    # prepare_data_train_example()
    # tokenizer_path = "../checkpoints/dtd/tokenizer/tokenizer.model"
    # saved_path = "..\\database\\dtd\\block_embedding\\finetune_data.pkl"
    # src_path = "U:\\binsearch_data\\train\\dtd"

    # tokenizer_path = "../checkpoints/dtd/tokenizer/tokenizer.model"
    tokenizer_path = "../checkpoints/dtd/tokenizer/tokenizer_type_1.ckp"

    save_path = "..\\database\\dtd\\block_embedding\\finetune_data_1.pkl"
    src_path = "U:\\binsearch_data\\train\\dtd\\luo"
    # prepare_train_0(n_anchor=0, tokenizer_path=tokenizer_path, src_path=src_path, saved_path=save_path)

    train_dir = "U:\\binsearch_data\\train\\dtd\\luo\\gcc"
    prepare_train_1(train_dir, tokenizer_path, save_path)