# -*-coding:utf-8-*-
import os
import sys
sys.path.append("..")
import pickle
import random
import numpy as np
import torch
import copy
from tqdm import tqdm
from library.model_libs import read_pickle, BlockEmbedding
from my_utils.file_utils import get_subpath_ext
from instruction_tokenizer import InstructionTokenizer

TLABEL = 1
FLABEL = -1


def read_data_gen_list(input_file=[], model_directory="./block_embedding/", tokenizer=None,
                       device=torch.device('cuda:0'),
                       saveName="", ProcessNum=0):
    BlkEmbedding = BlockEmbedding(model_directory=model_directory, tokenizer=tokenizer, device=device, batch_size=256)
    filelist = input_file
    data = []
    pbar = tqdm(filelist)
    for fname in pbar:
        func_dict = read_pickle(fname, blk_mdl=BlkEmbedding, min_blk=5, dim=128)
        data.extend(list(func_dict.values()))
        if saveName != "":
            pickle.dump(data, open(saveName, "wb"))
    return data


def gen_train_test_list(input_list: list, blk_mdl_path: str, save_path: str, device, tokenizer=None, n_neg=30, limit=3,
                        seed=10):
    BlkEmbedding = BlockEmbedding(model_directory=blk_mdl_path, tokenizer=tokenizer, device=device)
    data = []
    fname_1_dict = {}
    lst = random.choices(input_list, k=4)
    for file in lst:
        new_dict = read_pickle(file, blk_mdl=BlkEmbedding)
        fname_1_dict = dict(fname_1_dict, **new_dict)
    print("Total functions: ", len(fname_1_dict.keys()))
    randGen = np.random.RandomState(seed)
    keys = list(fname_1_dict.keys())
    for func in fname_1_dict:
        if func not in fname_1_dict:
            continue
        if len(fname_1_dict[func]) < limit or len(fname_1_dict[func]) < limit:
            continue
        count = n_neg
        while count:
            count -= 1
            negSample = randGen.choice(keys)
            if negSample == func:
                continue
            if len(fname_1_dict[negSample]) < limit:
                continue
            data.append((fname_1_dict[func], fname_1_dict[func], fname_1_dict[negSample]))
    if save_path != "":
        pickle.dump(data, open(save_path, "wb"))
        print("saved in {}".format(save_path))
    return data


def get_file_pairs_x_opt(root_path, max_pairs, file_pairs_path):
    _, all_O0_paths = get_subpath_ext(root_path=root_path, search_type="**/*", selected_suf=".pkl", only_path="O0")
    _, all_O1_paths = get_subpath_ext(root_path=root_path, search_type="**/*", selected_suf=".pkl", only_path="O1")
    _, all_O2_paths = get_subpath_ext(root_path=root_path, search_type="**/*", selected_suf=".pkl", only_path="O2")
    _, all_O3_paths = get_subpath_ext(root_path=root_path, search_type="**/*", selected_suf=".pkl", only_path="O3")
    anchor_names = [os.path.basename(x) for x in all_O0_paths]
    name2path = {}
    name2path["O0"] = dict(zip([os.path.basename(x) for x in all_O0_paths], all_O0_paths))
    name2path["O1"] = dict(zip([os.path.basename(x) for x in all_O1_paths], all_O1_paths))
    name2path["O2"] = dict(zip([os.path.basename(x) for x in all_O2_paths], all_O2_paths))
    name2path["O3"] = dict(zip([os.path.basename(x) for x in all_O3_paths], all_O3_paths))
    path_pair_lst = []
    n_pairs = 0
    for anchor in random.sample(anchor_names, min(len(anchor_names), max_pairs)):
        opts = ["O0", "O1", "O2", "O3"]
        opt1 = random.choice(opts)
        opts.remove(opt1)
        opt2 = random.choice(opts)
        path1 = name2path[opt1].get(anchor, None)
        path2 = name2path[opt2].get(anchor, None)
        if path1 and path2:
            path_pair_lst.append((path1, path2))
            n_pairs += 1
    print("max_pairs:{}, total file pairs: {}".format(max_pairs, n_pairs))
    pickle.dump(path_pair_lst, open(file_pairs_path, "wb"))
    print("dumping file pairs into {}".format(file_pairs_path))
    return path_pair_lst


def gen_train_x_optim(block_embedding_path, tokenizer_path,
                      path_pair_lst=None, file_pairs_path=None, save_path=None,
                      device=torch.device("cuda:0"), n_neg=16, n_anchor_per_file=8, seed=813):
    triplets = []
    blk_mdl = BlockEmbedding(model_directory=block_embedding_path, tokenizer=tokenizer_path, device=device)
    n_total = 0
    n_pair = 0
    randGen = np.random.RandomState(seed)
    if file_pairs_path:
        path_pair_lst = pickle.load(open(file_pairs_path, "rb"))
    for pair in path_pair_lst:
        file0, file1 = pair[0], pair[1]
        func_map0 = read_pickle(file0, blk_mdl=blk_mdl)
        func_map1 = read_pickle(file1, blk_mdl=blk_mdl)
        origin_0 = [x.split("_Hash_")[0] for x in func_map0.keys()]
        origin2name_0 = dict(zip(origin_0, list(func_map0.keys())))
        origin_1 = [x.split("_Hash_")[0] for x in func_map1.keys()]
        origin2name_1 = dict(zip(origin_1, list(func_map1.keys())))
        n_pair += 1
        origin_anchor_intersect = list(set(origin_0).intersection(set(origin_1)))
        if len(origin_anchor_intersect) < n_anchor_per_file:
            continue
        origin_anchors = random.choices(origin_anchor_intersect, k=n_anchor_per_file)
        for origin_anchor in origin_anchors:
            anchor_name = origin2name_0[origin_anchor]
            pos_name = origin2name_1[origin_anchor]
            count = 0
            origin_candidates = copy.deepcopy(origin_1)
            origin_candidates.remove(origin_anchor)
            neg_names = []
            while count < n_neg and len(origin_candidates) > 1:
                origin_neg = randGen.choice(origin_candidates)
                neg_names.append(origin2name_1[origin_neg])
                origin_candidates.remove(origin_neg)
                count += 1
            for neg in neg_names:
                triplets.append((func_map0[anchor_name], func_map1[pos_name], func_map1[neg]))
                n_total += 1
        # print("file1:{}, file2:{}\n\t n_anchor:{}, n_neg:{}".format(file0, file1, n_anchor_per_file, count))
    if save_path:
        print(
            "Dumping ... triplets to pkl.\nTotal file pairs: {}, Total triplets:{}, saved in {}".format(n_pair, n_total,
                                                                                                        save_path))
        pickle.dump(triplets, open(save_path, "wb"))


def validate(path):
    from function_embedding_train import read_data
    triplets = pickle.load(open(path, "rb"))
    print("total triplets: {}".format(len(triplets)))
    for t in triplets:
        if len(t) != 3:
            print("illegal triplet exists")
    print("pass")


if __name__ == '__main__':
    # input_files = [str(x) for x in Path(bin_parse_dir).glob("**/*.pkl")]
    # gen_train_test_list(input_list=input_files, blk_mdl_path=blk_mdl_path, tokenizer=tokenizer_path, device=torch.device('cuda:0'),
    #                     save_path=train_data_path)

    # blk_mdl_path = "../example/checkpoints/block_embedding/best"
    # tokenizer_path = "../example/checkpoints/tokenizer/tokenizer.model"
    # bin_parse_dir = "../example/database/binaries/"
    # train_data_path = "../example/database/function_embedding/function_embedding_data.pkl"

    # blk_mdl_path = "../checkpoints/luo/block_embedding/finetune/best"
    # tokenizer_path = "../checkpoints/luo/tokenizer/tokenizer.model"
    # bin_parse_dir = "U:/binsearch_data/train/luo/gcc"
    # train_data_path = "../database/luo/function_embedding/function_embedding_data.pkl"

    blk_mdl_path = "../checkpoints/dtd/block_embedding/finetune/best"
    tokenizer_path = "../checkpoints/dtd/tokenizer/tokenizer_type_0/tokenizer.model"
    bin_parse_dir = "U:/binsearch_data/train/dtd/luo/gcc"
    save_data_path = "../database/dtd/function_embedding/function_embedding_data.pkl"

    pair_lst = get_file_pairs_x_opt(root_path="U:\\binsearch_data\\train\\dtd\\luo\\gcc",
                                    file_pairs_path="../database/dtd/function_embedding/file_pairs.pkl",
                                    max_pairs=1e3)
    gen_train_x_optim(block_embedding_path=blk_mdl_path, tokenizer_path=tokenizer_path,
                      path_pair_lst=pair_lst, save_path=save_data_path,
                      n_anchor_per_file=4)

    validate(save_data_path)
