# -*-coding:utf-8-*-
import sys
sys.path.append("..")
from instruction_tokenizer import InstructionTokenizer
from my_utils.file_utils import get_files_ext
import pickle
from tqdm import tqdm


def main():
    # saved_path = "../example/database/block_embedding/pretrain_data.pkl"
    # bins_root = "../example/database/binaries"
    # tokenizer_ckpt = "../example/checkpoints/tokenizer/tokenizer.model"

    # saved_path = "../database/luo/block_embedding/pretrain_data.pkl"
    # bins_root = "U:\\binsearch_data\\train\\luo\\gcc"
    # tokenizer_ckpt = "../checkpoints/luo/tokenizer/tokenizer.model"

    saved_path = "../database/dtd/block_embedding/pretrain_data_1.pkl"
    bins_root = "U:\\binsearch_data\\train\\dtd"
    tokenizer_ckpt = "../checkpoints/dtd/tokenizer/tokenizer_type_1.ckp"

    tokenizer = pickle.load(open(tokenizer_ckpt, "rb"))
    file_lst = get_files_ext(root_path=bins_root, selected_suf='.pkl')
    min_blk = 5
    pbar = tqdm(file_lst)
    datasets = []
    for f in pbar:
        func_lst = pickle.load(open(f, "rb"))
        for func in func_lst:
            if len(func['blocks']) < min_blk:
                continue
            for cur, blk in zip(range(len(func['blocks'])), func['blocks']):
                instructions = func['blocks'][cur]['src']
                each_tokens = tokenizer(instructions, max_length=192, truncation=True)
                datasets.append(each_tokens)
    print("begin dumping dataset ...")
    pickle.dump(datasets, open(saved_path, "wb"))
    print("n_train: ", len(datasets))
    print("saved in {}".format(saved_path))


if __name__ == '__main__':
    main()


