# -*-coding:utf-8-*-
import os
import yaml
import sys
sys.path.append("..")
from my_utils.file_utils import get_files_ext
from my_utils.ida_utils import  call_ida
import time

curDir = os.path.abspath(os.path.dirname(__file__))
curDir = curDir.replace("\\", "/")
filtered_suf = (".i64", ".idb", ".pkl", ".py", ".txt", ".dat", ".a", ".sqlite", ".sqlite-crash", ".o", ".til", ".nam", ".id0", ".id1", ".id2")


def extract_features_by_dis(bins_root):
    ida_config_path = "../config/ida.yaml"
    ida_config = yaml.load(open(ida_config_path), Loader=yaml.FullLoader)
    cmd64 = ida_config['cmd64']
    script = curDir + "/extract_features.py"
    bins_lst = get_files_ext(root_path=bins_root, filtered_suf=filtered_suf)
    print("total files: {}, Ida parsing begin.".format(len(bins_lst)))
    # last_pause = "U:\\binsearch_data\\train\\luo"
    call_ida(ida_abs_path=cmd64, py_path=script, bin_paths=bins_lst, last_pause=None)
    print("total files: , ida parsing end.", len(bins_root))


if __name__ == '__main__':
    # bins_root = "U:\\binsearch_data\\train\\luo"
    # bins_root = "..\\example\\database\\binaries"
    bins_root = ["U:\\binsearch_data\\train\\dtd"]

    cur = time.time()
    extract_features_by_dis(bins_root)
    print("elapsed: ", time.time()-cur)
    # binaries_to_extracts()
