# -*-coding:utf-8-*-
import os
import sys
sys.path.append("..")
import pickle
from tqdm import tqdm
from elftools.elf.elffile import ELFFile
from my_utils.file_utils import get_files_ext


# Slightly modified below code:
# https://github.com/eliben/pyelftools/blob/master/examples/dwarf_decode_address.py
def decode_file_line(dwarfinfo, func_addrs):
    # Go over all the line programs in the DWARF information.
    ret = {}
    for CU in dwarfinfo.iter_CUs():
        # Look at line programs to find the file/line for the address
        lineprog = dwarfinfo.line_program_for_CU(CU)
        for entry in lineprog.get_entries():
            if entry.state is None:
                continue
            if entry.state.end_sequence:
                continue
            # We check if an address is in the given set of function addresses.
            cur_state = entry.state
            if cur_state.address not in func_addrs:
                continue

            file_name = b""
            try:
                file_name = lineprog["file_entry"][cur_state.file - 1].name
                # To obtain full path
                dir_name = CU.get_top_DIE().attributes["DW_AT_comp_dir"].value
                path_name = CU.get_top_DIE().attributes["DW_AT_name"].value
                file_name = os.path.join(dir_name, path_name)
            except:
                print("oh")

            if isinstance(file_name, bytes):
                file_name = file_name.decode()

            line = cur_state.line
            ret[cur_state.address] = (file_name, line)

    return ret

def decode_blk2line(dwarfinfo):
    ret = {}
    for CU in dwarfinfo.iter_CUs():
        # Look at line programs to find the file/line for the address
        lineprog = dwarfinfo.line_program_for_CU(CU)
        for entry in lineprog.get_entries():
            if entry.state is None:
                continue
            if entry.state.end_sequence:
                continue
            # We check if an address is in the given set of function addresses.
            cur_state = entry.state
            file_name = b""
            try:
                file_name = lineprog["file_entry"][cur_state.file - 1].name
                dir_name = CU.get_top_DIE().attributes["DW_AT_comp_dir"].value
                path_name = CU.get_top_DIE().attributes["DW_AT_name"].value
                file_name = os.path.join(dir_name, path_name)
                file_name = file_name.decode('utf-8').replace('\\', '/')
            except:
                print("oh")
            if isinstance(file_name, bytes):
                file_name = file_name.decode()
            line = cur_state.line
            ret[cur_state.address] = bytes(file_name + "_" + str(line), encoding='utf-8')
    return ret

def fetch_lineno(bin_name, func_addrs):
    addr_to_line = {}
    with open(bin_name, "rb") as f:
        elffile = ELFFile(f)
        if not elffile.has_dwarf_info():
            print("No Dwarf Found in ", bin_name)
        else:
            dwarf = elffile.get_dwarf_info()
            addr_to_line = decode_file_line(dwarf, func_addrs)

    return addr_to_line


def extract_dwarf(bin_name):
    if os.path.exists(bin_name + ".addr.dat"):
        print("{} exists".format(bin_name + ".addr.dat"))
        return 1
    else:
        with open(bin_name, "rb") as f:
            elffile = ELFFile(f)
            if not elffile.has_dwarf_info():
                print("No Dwarf Found in ", bin_name)
                return 0
            else:
                dwarf = elffile.get_dwarf_info()
                if dwarf.has_debug_info():
                    addr_to_line = decode_blk2line(dwarf)
                else:
                    return 0
    return addr_to_line



if __name__ == '__main__':
    # bins_root = "Z:\\binsearch_data\\train\\dtd"
    bins_root = ["U:\\binsearch_data\\train\\dtd\\1", "U:\\binsearch_data\\train\\dtd\\2"]
    # bins_root = "..\\database\\example"
    file_lst = get_files_ext(root_path=bins_root, filtered_suf=('.i64', '.txt', '.idb', ".id0", ".id1", "id2", ".id3", ".nam", ".dat", ".pkl", ".til"))
    bar = tqdm(file_lst)
    error_files = []
    n_has_debug = 0
    for f_path in bar:
        print(f_path)
        try:
            addr_to_line = extract_dwarf(f_path)
            if isinstance(addr_to_line,int):
                n_has_debug += 1
            else:
                pickle.dump(addr_to_line, open(f_path + ".addr.dat", "wb"))
        except:
            print("error: ", f_path)
            error_files.append(f_path)
    print(error_files)
    print("total:{}, has_debug:{}, error:{}".format(len(file_lst), n_has_debug, len(error_files)))

