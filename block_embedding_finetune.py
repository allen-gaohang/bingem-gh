import pickle
from pathlib import Path
from tqdm import tqdm
import numpy as np
import os
from transformers import RobertaModel
import torch
import torch.nn as nn
import torch.optim as optim
from sklearn.model_selection import train_test_split
import torch.utils.data as Data
import random
from instruction_tokenizer import InstructionTokenizer


class AssemblyLanguageModel:
    def __init__(self,
                 model_directory=None,
                 tokenizer=None,
                 device=torch.device('cuda:0')
                 ):
        self.tokenizer = pickle.load(open(tokenizer, "rb"))
        self.model = RobertaModel.from_pretrained(model_directory, add_pooling_layer=True)
        self.device = device

        self.model.to(self.device)
        self.istrain = False

    def __call__(self, *args, **kwargs):
        return self.forward(args[0])

    def parameters(self):
        return self.model.parameters()

    def train(self):
        self.istrain = True
        return self.model.train()

    def eval(self):
        self.istrain = False
        return self.model.eval()

    def save_pretrained(self, output_dir):
        self.model.save_pretrained(output_dir)

    def forward(self, inputs):
        ret = []
        if isinstance(inputs[0], (list, str)):
            tokens = self.tokenizer(inputs, return_tensors="pt", max_length=256, padding=True, truncation=True)[
                'input_ids']
        else:
            tokens = self.tokenizer.pad(inputs)['input_ids']
        tokens = tokens.to(self.device)
        data_loader = Data.DataLoader(tokens, batch_size=16)
        for batch_input in data_loader:
            output = self.model(batch_input)
            if self.istrain:
                ret.extend(list(output.last_hidden_state[:, -1, :]))
            else:
                ret.extend(list(output.last_hidden_state[:, -1, :].detach()))
        return torch.cat(ret, dim=0).to(self.device).reshape(len(ret), len(ret[0]))


P_norm = 2
triplet_criterion = nn.TripletMarginLoss(margin=9.5, p=P_norm, eps=1e-6)
entropy_loss = 0
Lambda = 1
ce_criterion = nn.CrossEntropyLoss()
criterion_mse = nn.MSELoss()

CUDA_LAUNCH_BLOCKING = 1


def train(model, train_inputs, device, optimizer, epoch, batch_size=64):
    model.train()
    total_iters = int(len(train_inputs)*epoch/batch_size)
    pbar = tqdm(range(total_iters), unit='batch')
    loss_accum = 0
    Lambda = random.random() * 1
    for pos in pbar:
        selected_idx = np.random.permutation(len(train_inputs))[:batch_size]
        batch_inputs = [train_inputs[idx] for idx in selected_idx]
        anchor_inputs = [_[0] for _ in batch_inputs]
        positive_inputs = [_[1] for _ in batch_inputs]
        negative_inputs = [_[2] for _ in batch_inputs]
        # print(anchor_inputs.shape)
        # print(positive_inputs.shape)
        anchor_outputs = model(anchor_inputs).to(device=torch.device(device))
        pos_outputs = model(positive_inputs).to(device=torch.device(device))
        neg_outputs = model(negative_inputs).to(device=torch.device(device))
        # assert anchor_outputs.shape == neg_outputs.shape
        # assert pos_outputs.shape == neg_outputs.shape
        pos_sim = 1 / (1 + torch.cdist(anchor_outputs, pos_outputs).diag())
        neg_sim = 1 / (1 + torch.cdist(anchor_outputs, neg_outputs).diag())
        p = torch.stack([1 - pos_sim, pos_sim], dim=-1)
        n = torch.stack([1 - neg_sim, neg_sim], dim=-1)
        preds = torch.cat((p, n), dim=0).to(device=torch.device(device))
        labels = torch.tensor([1] * len(pos_sim) + [0] * len(neg_sim))
        labels = labels.to(device=torch.device(device))
        loss_tri = triplet_criterion(anchor_outputs, pos_outputs, neg_outputs)
        loss_ce = ce_criterion(preds, labels)
        # loss_mse = criterion_mse(anchor_outputs, pos_outputs)
        loss = loss_tri + Lambda * loss_ce
        if optimizer is not None:
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
        loss = loss.detach().cpu().numpy()
        loss_accum += loss
        pbar.set_description('epoch: %d' % (epoch))
    average_loss = loss_accum / total_iters
    print("loss training: %f" % (average_loss))

    return average_loss


def test(model, test_inputs, batch_size=64):
    model.eval()
    total_iters = int(len(test_inputs) / batch_size)
    pbar = tqdm(range(total_iters), unit='batch')
    TP, all_case = 0, 0
    for pos in pbar:
        selected_idx = np.random.permutation(len(test_inputs))[:batch_size]
        batch_inputs = [test_inputs[idx] for idx in selected_idx]
        anchor_inputs = [_[0] for _ in batch_inputs]
        positive_inputs = [_[1] for _ in batch_inputs]
        negative_inputs = [_[2] for _ in batch_inputs]

        anchor_outputs = model(anchor_inputs).detach()
        positive_outputs = model(positive_inputs).detach()
        negative_outputs = model(negative_inputs).detach()

        Pred = torch.pairwise_distance(anchor_outputs, negative_outputs, p=P_norm) \
               - torch.pairwise_distance(anchor_outputs, positive_outputs, p=P_norm)
        all_case += len(Pred)
        TP += len(Pred[torch.where(Pred > 0)])
    accuracy = TP / all_case
    # distance_avg = Pred.sum() / all_case
    # print("Distance", distance_avg)
    print("Accuracy", accuracy)
    return accuracy


def load_dataset(data_dir):
    ret = []
    data_files = [str(x) for x in Path(data_dir).glob("*.pkl")]
    for f in data_files:
        data_ = pickle.load(open(f, "rb"))
        ret.extend(data_)
    return ret


def main():
    # model_directory = "example/checkpoints/block_embedding/pretrain/best"
    # tokenizer_directory = "example/checkpoints/tokenizer/tokenizer.model"
    # train_data_path = "example/database/block_embedding/finetune_data.pkl"
    # model_output_dir = "example/checkpoints/block_embedding/finetune/"

    # model_directory = "./checkpoints/luo/block_embedding/pretrain/best"
    # tokenizer_directory = "./checkpoints/luo/tokenizer/tokenizer.model"
    # train_data_path = "./database/luo/block_embedding/finetune_data.pkl"
    # model_output_dir = "./checkpoints/luo/block_embedding/finetune/"

    model_directory = "./checkpoints/dtd/block_embedding/pretrain/best"
    tokenizer_directory = "./checkpoints/dtd/tokenizer/tokenizer_type_1.ckp"
    train_data_path = "./database/dtd/block_embedding/finetune_data.pkl"
    model_output_dir = "./checkpoints/dtd/block_embedding/finetune/"

    epochs = 4
    lr = 1e-3
    device = torch.device('cuda:0')

    best_results = 0.95
    inputs = pickle.load(open(train_data_path, "rb"))
    train_inputs, test_inputs = train_test_split(inputs, train_size=0.8, test_size=0.2, random_state=66, shuffle=True)
    print("Train:", len(train_inputs), "\tTest:", len(test_inputs))
    model = AssemblyLanguageModel(model_directory=model_directory, tokenizer=tokenizer_directory, device=device)
    optimizer = optim.Adam(model.parameters(), lr=lr)
    scheduler = optim.lr_scheduler.StepLR(optimizer, step_size=8, gamma=0.9)

    for epoch in range(1, epochs + 1):
        train(model, train_inputs, device, optimizer, epoch, batch_size=64)
        scheduler.step()
        current_result = test(model, test_inputs)
        if current_result > best_results:
            output_dir = model_output_dir + str(current_result) + "_" + str(scheduler.get_last_lr()[-1])
            best_results = current_result
            if not os.path.exists(output_dir):
                os.makedirs(output_dir)
            else:
                print("path:{} exists")
            model.save_pretrained(output_dir)


if __name__ == '__main__':
    main()
